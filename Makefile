VERSION=v0.1.0

clean:
	@rm -rf target

run:
	@clj -m bank-account.core

test:
	@clj -A:test

uberjar:
	@clj -A:uberjar

run_uberjar:
	@java -cp target/bank-account.jar clojure.main -m bank-account.core

docker:
	@sudo docker build -t bank-account:$(VERSION) -f deploy/Dockerfile .

run_docker:
	@sudo docker run bank-account:$(VERSION)

.PHONY: test run uberjar docker run_uberjar run_docker

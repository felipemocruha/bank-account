(ns bank-account.spec
  (:require [spec-tools.data-spec :as ds]
            [clojure.spec.alpha :as s])
  (:gen-class))

(def transfer-request
  {::event-id string?
   ::source-account-id string?
   ::dest-account-id string?
   ::amount pos-int?})

(def transfer-requests-spec
  (ds/spec
   {:name ::transfer-request
    :spec transfer-request}))

(s/def ::transfer-response-error #{:no-funds :timeout}

(def transfer-response
  {::event-id string?
   ::success bool?
   ::error ::transfer-response-error})

(def balance-decrement-request
  {::event-id string?
   ::account-id string?
   ::amount pos-int?
   ::response-topic string?})

(def balance-decrement-request-spec
  (ds/spec
   {:name ::balance-decrement-request
    :spec balance-decrement-request}))

(s/def ::balance-decrement-error #{:no-funds :timeout})

(def balance-decrement-response
  {::event-id string?
   ::success boolean?
   ::error ::balance-decrement-error
   ::account-id string?})

(def balance-decrement-response-spec
  (ds/spec
   {:name ::balance-decrement-response
    :spec balance-decrement-response}))

(def balance-increment-request
  {::event-id string?
   ::account-id string?
   ::amount pos-int?
   ::response-topic string?})

(def balance-increment-request-spec
  (ds/spec
   {:name ::balance-increment-request
    :spec balance-increment-request}))

(s/def ::balance-increment-error #{:timeout})

(def balance-increment-response
  {::event-id string?
   ::success boolean?
   ::error ::balance-increment-error
   ::account-id string?})

(def balance-increment-response-spec
  (ds/spec
   {:name ::balance-increment-response
    :spec balance-increment-response}))

(ns bank-account.core
  (:require [reitit.http :as http]
            [reitit.ring :as ring]
            [reitit.interceptor.sieppari :as sieppari]
            [aleph.http :as aleph]
            [muuntaja.core :as m]
            [taoensso.timbre :as log]
            [bank-account.metrics :as metrics])
  (:gen-class))

(defn mkresponse [body]
  (m/encode "application/edn" body))

(defn decode-body [req]
  (m/decode "application/edn" (:body req)))

(defn healthcheck [req]
  {:status 200 :body (mkresponse {:message "ok"})})

(defn uuid [] (.toString (java.util.UUID/randomUUID)))

(def routes
  [["/healthcheck"
    {:interceptors [metrics/http-metrics-interceptor]
     :handler healthcheck}]])

(def app (http/ring-handler
          (http/router routes)
          (ring/create-default-handler)
          {:executor sieppari/executor}))

(def server-opts {:host "0.0.0.0"
                  :port 8080})

(defn start-server [opts]
  (log/info (format "starting server at: %s:%d"
                    (:host opts) (:port opts)))
  (aleph/start-server app opts))

(defn log-formatter [{:keys [level instant msg_
                             ?file ?line hostname_]}]
  (-> {:level level
       :timestamp (quot (.getTime instant) 1000)
       :message (force msg_)
       :file ?file
       :line ?line
       :hostname (force hostname_)}
      .toString))

(defn -main [& args]
  (log/merge-config! {:output-fn log-formatter})  
  (start-server server-opts))

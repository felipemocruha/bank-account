(ns bank-account.metrics
  (:require [iapetos.core :as prom]
            [sieppari.core :as sp]
            [iapetos.standalone :as standalone]
            [iapetos.registry :as r]
            [taoensso.timbre :as log])
  (:gen-class))

(def http-requests-total
  (prom/counter
    :bank-account/http-requests-total
    {:description "requests count by method/path/status."
     :labels [:method :path :status]}))

(def http-request-duration-seconds
  (prom/histogram
   :bank-account/http-request-duration-seconds
   {:description "request duration in seconds by method/path/status."
    :labels [:method :path :status]}))

(defonce metrics
  (-> (prom/collector-registry)
      (prom/register
       http-requests-total
       http-request-duration-seconds)))

(defn now-unix [] (quot (System/currentTimeMillis) 1000))

(defn- http-metrics-interceptor-enter [ctx]
  (let [req (:request ctx)]
    (assoc req :started-at (now-unix))))

(defn- http-metrics-interceptor-leave [ctx]
  (let [req (:request ctx)
        resp (:response ctx)
        labels {:method (:method resp)
                :status (:status resp)
                :path (:path req)}]
    (log/info "ctx: " ctx)
    (log/info "req: " req)
    (log/info "labels: " labels)
    (prom/inc (metrics :bank-account/http-requests-total labels))
    (prom/observe
     (metrics :bank-account/http-request-duration-seconds labels)
     (- (now-unix) (:started-at ctx)))))

(def http-metrics-interceptor
  {:enter http-metrics-interceptor-enter})
;   :leave http-metrics-interceptor-leave})

(defonce metrics-server
  (standalone/metrics-server metrics {:port 8081}))
